var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: "./src/index.tsx",
    output: {
        filename: "bundle.js",  
        path: __dirname + "/dist"
    },

    devtool: "source-map",

    devServer: { 
        inline: true,
        
    },

    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".scss"]
    },

    module: {
        loaders: [{
            test: /\.tsx?$/, loader: "awesome-typescript-loader"
        }, {
            test: /\.scss$/,
            loaders: ["style-loader", "css-loader?importLoaders=1", "postcss-loader","sass-loader"]
        },  
        ],


        preLoaders: [
            { test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    plugins: [
        // new CopyWebpackPlugin([
        //     { from: 'src/assets', to: 'assets' }
        // ])
    ],
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};