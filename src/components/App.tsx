import * as React      from 'react'
import initialState    from '../api/initialState'
import                      './App.scss'

const times = [
    '6:00 AM',
    '7:00 AM',
    '8:00 AM',
    '9:00 AM',
    '10:00 AM',
    '11:00 AM',
    '12:00 PM',
    '1:00 PM',
    '2:00 PM',
]

const products = [
    'GP',
    'HE',
    'FLY ASH'
]

const silos = [
    'Silo 1',
    'Silo 2',
    'Silo 3',
]

const status = [
    'draft',
    'ordered'
]


const Option = function(props : any) {
    return(
        <option value={props.value}>{props.value}</option>
    )
}

const Order = function(props : any) {
    const { time, product, silo, quantity, status, updateOrder, index } = props
    return (
        <tr>
            <td>{time} </td>
            <td>{product}</td>
            <td>{silo}</td>
            <td>{quantity}</td>
            <td>{status}</td>
            <td>
                <button onClick={()=>{updateOrder(index, -1)}} >Earlier</button>
                <button onClick={()=>{updateOrder(index, +1)}} >Later</button>
                <button onClick={()=>{updateOrder(index, 0)}} >Cancel</button>
            </td>
        </tr>
    )
}

export default class App extends React.Component<any, any> {
    newTime: any
    newProduct: any
    newSilo: any
    newQuantity: any
    newStatus: any
    constructor (props : any) {
        super(props)
        this.state = initialState
    this.newOrder = this.newOrder.bind(this)
    this.updateOrder = this.updateOrder.bind(this)
    }

    newOrder(event : any) {
        const values = {
            time: this.newTime.value,
            product: this.newProduct.value,
            silo: this.newSilo.value,
            quantity: this.newQuantity.value,
            status: this.newStatus.value,
        }
        let newState = this.state.orders
        newState.push(values)
        this.setState(newState)
        this.reset()
    }
    updateOrder(index: number, type: number) {
        let newState = this.state.orders
        if (type === 0 ) newState[index].status = 'Cancelled'
        else {
            newState[index].time = times[times.indexOf(newState[index].time) + type]
        }
        this.setState(newState)
    }
    reset() {
        this.newTime.value= ''
        this.newProduct.value= ''
        this.newSilo.value= ''
        this.newQuantity.value= ''
        this.newStatus.value= ''
    }

    render () {
        const today = 'February 20th 2017'
        return(
            <div>
                <h1>Orders</h1>
                <h2>Today: { today }</h2>
                <table className="today-table">
                    <thead>
                        <tr>
                            <td>Time Requested</td>
                            <td>Product</td>
                            <td>Silo</td>
                            <td>Quantity</td>
                            <td>Status</td>
                        </tr>
                        { this.state.orders.map((order, index)=><Order {...order } updateOrder={ this.updateOrder }index={index}/>)}
                    </thead>    
                    <tbody>
                    <tr>
                            <td>
                                <select className="new-time" ref={(input) => { this.newTime = input }}>
                                    <option>Time Requested</option>
                                    { times.map(value=><Option value={ value }/>)}
                                </select>
                            </td>
                            <td>
                                <select className="new-product" ref={(input) => { this.newProduct = input; }}>
                                
                                    <option>Product</option>
                                    { products.map(value=><Option value={ value }/>)}
                                
                                </select>
                            </td>
                            <td>
                                <select className="new-silo" ref={(input) => { this.newSilo = input; }}>
                                
                                <option>Silo</option>
                                     { silos.map(value=><Option value={ value }/>)}
                                </select>
                            </td>
                            <td>
                                <select className="new-quantity"  ref={(input) => { this.newQuantity = input; }}>
                                
                                <option>Quantity</option>
                                <option>Tanker</option>
                                </select>
                            </td>
                            <td>
                                <select className="new-status" ref={(input) => { this.newStatus = input; }} onInput={ this.newOrder }>
                                
                                Status
                                     { status.map(value=><Option value={value}/>)}
                                
                                </select>
                            </td>
                    </tr>
                    </tbody>
                    
                </table>
            </div>
        )
    }
}